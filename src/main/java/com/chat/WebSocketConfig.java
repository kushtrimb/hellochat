/**
 * Created by Kushtrim on 5/14/2017.
 */
package com.chat;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {


    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry wsh) {
        wsh.addHandler(new TuDateHandler(), "/chat").setAllowedOrigins("*");
    }

    class TuDateHandler extends TextWebSocketHandler {

        @Override
        public void afterConnectionEstablished(WebSocketSession session) throws Exception {
            String name = "Test";
            System.out.println("opening...... " + name);
        }

        @Override
        public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {


        }

        @Override
        public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
            String name = "Test";
            System.out.println("closing...... " + name);
        }

    }

}